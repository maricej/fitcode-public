<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'markg949_fcwp01');

/** MySQL database username */
define('DB_USER', 'markg949_fcwp01');

/** MySQL database password */
define('DB_PASSWORD', '39b!4SphW@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q70vcxhitoyoisicrahkzpzse8kuujlap5vcbonnyxnzytuzlkfdkanvvibnvdui');
define('SECURE_AUTH_KEY',  'crmycroqsee5vsimquygdracecdar5ovm7o2vykbfbiyycy6swksn6whdwdll9u8');
define('LOGGED_IN_KEY',    'vtlalrhagworrwodedvlfktqfembkeukfxywgmpotisvrvqcaeiuhuebdt7bvcwq');
define('NONCE_KEY',        'pf0gtxpfxmcnz8ngp1z0t5cotrupif0rhzvnqxszi2phkwrqjytojnwpkfwl8n42');
define('AUTH_SALT',        'f1zyjg7i13btxwvwswfcgwndve97fr6197w279okxflq2ar5bizavqentyx2ebxl');
define('SECURE_AUTH_SALT', 'oumfwruod2mofl8mitvflcoboddts43dtbzzmhbflhf3a72ubxszvxmam4beomh6');
define('LOGGED_IN_SALT',   'fqbih0hb2ccqfbzjy1wnxiyqn1c1ojymprfovaucewairfwdmvr0enwowmksxuzi');
define('NONCE_SALT',       'goprpgyvnbatit7gwzisncsjrusdxcoysuoo2s9ffknrt7vsy8x9rs4gymgugql8');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'fcwp01_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define( 'WP_MEMORY_LIMIT', '128M' );
define( 'WP_AUTO_UPDATE_CORE', false );

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'fitcodehq.com');
define('PATH_CURRENT_SITE', '/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
