<?php
ini_set('error_reporting', E_STRICT);
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) exit;

class UM_REST_API {

	const VERSION = '1.0';

	private $pretty_print = false;
	public 	$log_requests = true;
	private $is_valid_request = false;
	private $user_id = 0;
	private $stats;
	private $data = array();
	private $override = true;

	public function __construct() {
		
		add_action( 'init',                     array( $this, 'add_endpoint'     ) );
		add_action( 'template_redirect',        array( $this, 'process_query'    ), -1 );
		add_filter( 'query_vars',               array( $this, 'query_vars'       ) );
		add_action( 'show_user_profile',        array( $this, 'user_key_field'   ) );
		add_action( 'edit_user_profile',        array( $this, 'user_key_field'   ) );
		add_action( 'personal_options_update',  array( $this, 'update_key'       ) );
		add_action( 'edit_user_profile_update', array( $this, 'update_key'       ) );

		// Determine if JSON_PRETTY_PRINT is available
		$this->pretty_print = defined( 'JSON_PRETTY_PRINT' ) ? JSON_PRETTY_PRINT : null;

		// Allow API request logging to be turned off
		$this->log_requests = apply_filters( 'um_api_log_requests', $this->log_requests );

	}

	/**
	 * Registers a new rewrite endpoint for accessing the API
	 */
	public function add_endpoint( $rewrite_rules ) {
		add_rewrite_endpoint( 'um-api', EP_ALL );
	}

	/**
	 * Registers query vars for API access
	 */
	public function query_vars( $vars ) {

		$vars[] = 'key';
		$vars[] = 'token';
		$vars[] = 'format';
		$vars[] = 'query';
		$vars[] = 'type';
		$vars[] = 'data';
		$vars[] = 'fields';
		$vars[] = 'value';
		$vars[] = 'number';
		$vars[] = 'id';
		$vars[] = 'email';
		$vars[] = 'orderby';
		$vars[] = 'order';
		$vars[] = 'include';
		$vars[] = 'exclude';
		$vars[] = 'username';
		$vars[] = 'password';
		$vars[] = 'role';
		$vars[] = 'community_role';
		$vars[] = 'vendor';
		$vars[] = 'name';
		$vars[] = 'trainor_id';
		$vars[] = 'recipient';
		$vars[] = 'author';
		$vars[] = 'group_id';
		$vars[] = 'message_content';
		$vars[] = 'groupMembers';
		$vars[] = 'details';
		$vars[] = 'event_id';
		$vars[] = 'event_name';
		$vars[] = 'event_start_date';
		$vars[] = 'event_end_date';
		$vars[] = 'event_location';
		$vars[] = 'client_id';
		$vars[] = 'status';
		$vars[] = 'program';
		$vars[] = 'industry';
		$vars[] = 'tags';	
		$vars[] = 'description';
		$vars[] = 'week_plan';

		$vars[] = 'trainer_id';

		//vars for demo and user registration//
		$vars[] = 'fname';
		$vars[] = 'lname';
		$vars[] = 'companyName';
		$vars[] = 'country';
		$vars[] = 'industry';
		$vars[] = 'phone';
		// end vars for demo and user registration// 

		//vars for workout 
		$vars[]= 'notes';
		$vars[]= 'exercises';
		$vars[]= 'no_weeks';
		// emd vars for workout
		$vars[] = 'created_by';
		$vars[] = 'created_to';
		$vars[] = 'date';
		$vars[] = 'type';
		$vars[] = 'datas';
		$vars[] = 'gender';
		$vars[] = 'groupids';
		$vars[] = 'user_type';
		$vars[] = 'action';



		$this->vars = $vars;

		return $vars;
	}

	/**
	 * Validate the API request
	 */
	private function validate_request() {
		global $wp_query;

		$this->override = false;

        // Make sure we have both user and api key
		if ( ! empty( $wp_query->query_vars['um-api'] ) ) {

			if ( empty( $wp_query->query_vars['token'] ) || empty( $wp_query->query_vars['key'] ) )
				$this->missing_auth();

			// Retrieve the user by public API key and ensure they exist
			if ( ! ( $user = $this->get_user( $wp_query->query_vars['key'] ) ) ) :
				$this->invalid_key();
			else :
				$token  = urldecode( $wp_query->query_vars['token'] );
				$secret = get_user_meta( $user, 'um_user_secret_key', true );
				$public = urldecode( $wp_query->query_vars['key'] );

				if ( hash_equals( md5( $secret . $public ), $token ) )
					$this->is_valid_request = true;
				else
					$this->invalid_auth();
			endif;
		}
	}

	/**
	 * Retrieve the user ID based on the public key provided
	 */
	public function get_user( $key = '' ) {
		global $wpdb, $wp_query;

		if( empty( $key ) )
			$key = urldecode( $wp_query->query_vars['key'] );

		if ( empty( $key ) ) {
			return false;
		}

		$user = get_transient( md5( 'um_api_user_' . $key ) );

		if ( false === $user ) {
			$user = $wpdb->get_var( $wpdb->prepare( "SELECT user_id FROM $wpdb->usermeta WHERE meta_key = 'um_user_public_key' AND meta_value = %s LIMIT 1", $key ) );
			set_transient( md5( 'um_api_user_' . $key ) , $user, DAY_IN_SECONDS );
		}

		if ( $user != NULL ) {
			$this->user_id = $user;
			return $user;
		}

		return false;
	}

	// Custom function to check if the user is valid
	public function is_user(){
		global $wpdb, $wp_query;
		$username = $wp_query->query_vars['username'];
		$password = $wp_query->query_vars['password'];

		return wp_authenticate_username_password( '', $username, $password );

	}

	public function reset_password(){
		global $wpdb, $wp_query;
		$username = $wp_query->query_vars['username'];

		return fitcode_reset_password_process_hook($username);

	}

	public function change_password(){
		global $wpdb, $wp_query;
		$id = $wp_query->query_vars['id'];
		$password = $wp_query->query_vars['password'];
		$user['ID'] = $id;
		$user['user_pass'] = $password;
		return wp_update_user($user);
	}


	public function add_members(){
		global $wpdb, $wp_query;
		$user['user_login'] = $wp_query->query_vars['email'];
		$user['email'] = $wp_query->query_vars['email'];
		$user['user_pass'] = $wp_query->query_vars['password'];
		$user['display_name'] = $wp_query->query_vars['name'];
		$user['role'] = "Client";
		$user['community_role'] = "Client";

		$new_user_id = wp_insert_user($user);

		if(!$new_user_id->errors["existing_user_login"]){
			$key = "user_trainer_id";
			$value = $wp_query->query_vars['trainor_id'];
			update_metadata('user', $new_user_id, $key, $value, "");
			do_action( 'um_after_user_updated', $new_user_id );
			return true;
		}else{
			return false;
		}
			
	}

	public function get_members(){
		global $wpdb, $wp_query;

		$user_query = new WP_User_Query( array( 'meta_key' => 'user_trainer_id', 'meta_value' => $wp_query->query_vars['id'] ) );
		$res = $user_query->get_results();
		$counter = 0;
		foreach ($res as $data) {
			$response = null;
			$id = $data->ID;
			
			um_fetch_user($id);
			//$response[$counter]['id'] = um_profile('ID');
			$response[$counter]['gender'] = um_profile('user_gender');
			$response[$counter]['bday'] = um_profile('user_birthday');
			$response[$counter]['height'] = um_profile('user_height');
			$response[$counter]['bodyweight'] = um_profile('user_bodyweight');
			$response[$counter]['waistline'] = um_profile('user_waistline');
			$response[$counter]['mobile_no'] = um_profile('mobile_number');
			$response[$counter]['last_online'] = um_profile('_um_last_login');
			$response[$counter]['user_address'] = um_profile('user_address');
			$response[$counter]['name'] = um_profile('first_name')." ".um_profile('last_name');
			$counter += 1;
			$data->details = $response;
		}

		return  $res;
	}

	public function get_users_by_role(){
		global $wpdb, $wp_query;
		$com_role =  $wp_query->query_vars['community_role'];

		$user_query = new WP_User_Query( array( 'community_role' => $com_role ) );
		$res =  $user_query->get_results();
		$counter = 0;
		foreach ($res as $data) {
			$response = null;
			$id = $data->ID;

			um_fetch_user( $id );
			$response[$counter]['id'] = um_profile('ID');
			$response[$counter]['user_address'] = um_profile('user_address');
			$response[$counter]['user_lng'] = um_profile('user_lng');
			$response[$counter]['user_lat'] = um_profile('user_lat');
			$response[$counter]['fist_name'] = um_profile('first_name');
			$response[$counter]['last_name'] = um_profile('last_name');
			$response[$counter]['user_nicename'] = um_profile('user_nicename');
			$counter += 1;
			$data->details = $response;
		}

		return  $res;
	}

	public function update_profile(){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];
		$meta_key = explode( ',', $wp_query->query_vars['fields']);
		$meta_value = explode(',', $wp_query->query_vars['value']);

		$meta_data = array_combine($meta_key, preg_replace('/[^A-Za-z0-9\. -]/', '', $meta_value));

		foreach($meta_data as $key => $value){
			if($key == "user_height" || $key == "user_weight" || $key == "user_waistline") $value = intval($value);
		  	update_metadata('user', $user_id, $key, $value, "");
		}
		do_action( 'um_after_user_updated', $user_id );
		return true;
	}

	public function get_all_clients(){
		global $wpdb, $wp_query;
		$trainer_id =  $wp_query->query_vars['id'];

		$query = "SELECT user_id FROM {$wpdb->prefix}user_trainers WHERE trainer_id = $trainer_id";
		$client_lists = $wpdb->get_results( $query );
		foreach ($client_lists as $client) {
			$response = null;
			$id = $client->user_id;
			//$client = null;
			um_fetch_user($id);
			//$response[$counter]['id'] = um_profile('ID');
			if($id!=null){
				$client->data->ID = $id ? $id : "";
				$client->data->user_login = um_profile('user_login') ? um_profile('user_login') : "";
				$client->data->user_nicename = um_profile('user_nicename');
				$client->data->user_email = um_profile('user_email');
				$client->data->user_url = um_profile('user_url');
				$client->data->user_registered = um_profile('user_registered');
				$client->data->user_status = um_profile('user_status');
				$client->data->display_name = um_profile('display_name');

				$response['gender'] = um_profile('user_gender');
				$response['bday'] = um_profile('user_birthday');
				$response['height'] = um_profile('user_height');
				$response['bodyweight'] = um_profile('user_bodyweight');
				$response['waistline'] = um_profile('user_waistline');
				$response['mobile_no'] = um_profile('mobile_number');
				$response['last_online'] = um_profile('_um_last_login');
				$response['user_address'] = um_profile('user_address');
				$response['name'] = um_profile('first_name')." ".um_profile('last_name');
				unset($client->user_id);
				$client->data->details = $response;
				$client->ID = $id;
			}
			
			
		}
		// $client_lists = array_remove_keys($client_lists, 'user_id');
		return  $client_lists;
	}


	public function get_all_trainers(){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];

		$query = "SELECT trainer_id FROM {$wpdb->prefix}user_trainers WHERE user_id = $user_id";
		$client_lists = $wpdb->get_results( $query );
		foreach ($client_lists as $client) {
			$response = null;
			$id = $client->trainer_id;
			//$client = null;
			um_fetch_user($id);
			//$response[$counter]['id'] = um_profile('ID');
			if($id!=null){
				$client->data->ID = $id ? $id : "";
				$client->data->user_login = um_profile('user_login') ? um_profile('user_login') : "";
				$client->data->user_nicename = um_profile('user_nicename');
				$client->data->user_email = um_profile('user_email');
				$client->data->user_url = um_profile('user_url');
				$client->data->user_registered = um_profile('user_registered');
				$client->data->user_status = um_profile('user_status');
				$client->data->display_name = um_profile('display_name');

				$response['gender'] = um_profile('user_gender');
				$response['bday'] = um_profile('user_birthday');
				$response['height'] = um_profile('user_height');
				$response['bodyweight'] = um_profile('user_bodyweight');
				$response['waistline'] = um_profile('user_waistline');
				$response['mobile_no'] = um_profile('mobile_number');
				$response['last_online'] = um_profile('_um_last_login');
				$response['user_address'] = um_profile('user_address');
				$response['name'] = um_profile('first_name')." ".um_profile('last_name');
				unset($client->user_id);
				$client->data->details = $response;
				$client->ID = $id;
			}
			
			
		}
		// $client_lists = array_remove_keys($client_lists, 'user_id');
		return  $client_lists;
	}

	public function acceptAsClient(){
		global $wpdb, $wp_query;
		$trainer_id =  $wp_query->query_vars['id'];
		$client_id = $wp_query->query_vars['client_id'];

		$query = "INSERT INTO {$wpdb->prefix}user_trainers (`user_id`, `trainer_id`) VALUES ('$client_id','$trainer_id')";
		$query = $wpdb->query( $query );
		$id = $wpdb->insert_id;
		$response['results'] = $id;

		return $response;

	}

	public function getYourGroups( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id']; 

		$query = "SELECT id,name,cover_link FROM `fcWP_groups` WHERE `groupMembers` LIKE '%$user_id%'";
		$groups = $wpdb->get_results( $query );
		$response['results'] = $groups;

		return $response;
	}

	public function favorite_group( $args ){
		global $wpdb, $wp_query;
		$group_id =  $wp_query->query_vars['id']; 
		$status = $wp_query->query_vars['status'];

		$query = "UPDATE `fcWP_groups` SET `favorite` = '$status' WHERE `id` = '$group_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;
	}

	public function get_user_details( $args ){
		global $wpdb, $wp_query, $ultimatemember;
		$user_id =  $wp_query->query_vars['id'];

		$response = [];

		um_fetch_user($user_id);
		$response['id'] = $user_id;
		$response['user_login'] = um_profile('user_login') ? um_profile('user_login') : "";
		$response['user_nicename'] = um_profile('user_nicename');
		$response['user_email'] = um_profile('user_email');
		$response['profile_pic'] = $this->getsrc( um_user('profile_photo', 200) );
		$response['user_status'] = um_profile('user_status');
		$response['display_name'] = um_profile('display_name');

		$response['gender'] = um_profile('user_gender');
		$response['bday'] = um_profile('user_birthday');
		$response['height'] = um_profile('user_height');
		$response['bodyweight'] = um_profile('user_bodyweight');
		$response['waistline'] = um_profile('user_waistline');
		$response['mobile_no'] = um_profile('mobile_number');
		$response['last_online'] = um_profile('_um_last_login');
		$response['user_address'] = um_profile('user_address');
		$response['name'] = um_profile('first_name')." ".um_profile('last_name');

		return $response;

	}

	/*************
	 * Messaging *
	 *************/
	public function get_conversation( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];

		$response['results']['conversations'] = "This API required UM Messaging API";
		if ( class_exists( 'UM_Messaging_API') ) {
			$query = "SELECT * FROM {$wpdb->prefix}um_conversations WHERE user_a LIKE '%$user_id%' OR user_b = '$user_id'";
			$conversations = $wpdb->get_results( $query );
			$response['results']['conversations'] = $conversations;
		}
		return $response;
	 }

	public function get_message( $args ){
		global $wpdb, $wp_query;
		$conversation_id =  $wp_query->query_vars['id'];

		$response['results']['messages'] = "This API required UM Messaging API";
		if ( class_exists( 'UM_Messaging_API') ) {
			$query = "SELECT * FROM {$wpdb->prefix}um_messages WHERE conversation_id = '$conversation_id'";
			$conversations = $wpdb->get_results( $query );
			$response['results']['messages'] = $conversations;
		}
		return $response;
	}

	public function get_group_conversation( $args ){
		global $wpdb, $wp_query;

		$user_id = $wp_query->query_vars['id'];
		$group_id = $wp_query->query_vars['group_id'];

		$query = "SELECT * FROM {$wpdb->prefix}group_conversation WHERE `owner` = '$user_id' OR `group_id` = '$group_id'";
		$conversations = $wpdb->get_results( $query );
		$response['results']['conversations'] = $conversations;

		return $response;
	}

	public function get_group_message( $args ){
		global $wpdb, $wp_query;

		$conversation_id =  $wp_query->query_vars['id'];

		$query = "SELECT * FROM {$wpdb->prefix}group_messaging WHERE group_conversation_id = '$conversation_id'";
		$conversations = $wpdb->get_results( $query );
		$response['results']['messages'] = $conversations;
		return $response;
	}
	public function get_last_group_message( $args ){
		global $wpdb, $wp_query;

		$conversation_id =  $wp_query->query_vars['id'];

		$query = "SELECT * FROM {$wpdb->prefix}group_messaging  WHERE group_conversation_id = '$conversation_id'
		ORDER BY id desc LIMIT 1";
		$conversations = $wpdb->get_results( $query );
		$response['results']['messages'] = $conversations;
		return $response;
	}

	public function insert_conversation_to_group( $args ){
		global $wpdb, $wp_query;
		$recipient_group_id =  $wp_query->query_vars['recipient'];
		$author_trainer_id =  $wp_query->query_vars['author'];

		$query = "INSERT INTO {$wpdb->prefix}group_conversation (`id`, `group_id`, `owner`, `last_update`) VALUES (NULL,'$recipient_group_id','$author_trainer_id',NOW())";
		$query = $wpdb->query( $query );
		$conversation_id = $wpdb->insert_id;
		$response['results'] = $conversation_id;

		return $response;
	}

	public function insert_message_to_group( $args ){
		global $wpdb, $wp_query;
		$message_content = $wp_query->query_vars['message_content'];
		$group_conversation_id =  $wp_query->query_vars['id'];
		$recipient =  $wp_query->query_vars['recipient'];
		$author_trainer_id =  $wp_query->query_vars['author'];

		$query = "INSERT INTO {$wpdb->prefix}group_messaging (`id`, `group_conversation_id`, `content`, `author`, `reciepient`, `time`) VALUES (NULL,'$group_conversation_id','$message_content','$author_trainer_id','$recipient',NULL)";
		$query = $wpdb->query( $query );
		$message_id = $wpdb->insert_id;

		$query = "UPDATE {$wpdb->prefix}group_conversation SET last_update = NOW() WHERE id = '$group_conversation_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;
	}

	public function insert_conversation( $args ){
		global $wpdb, $wp_query;
		$recipient =  $wp_query->query_vars['recipient'];
		$author =  $wp_query->query_vars['author'];

		$response['results'] = "This API required UM Messaging API";
		if ( class_exists( 'UM_Messaging_API') ) {
			$query = "INSERT INTO {$wpdb->prefix}um_conversations (user_a,user_b,last_updated) VALUES ('$recipient','$author',NOW())";
			$query = $wpdb->query( $query );
			$conversation_id = $wpdb->insert_id;

			$response['results'] = $conversation_id;
		}
		return $response;
	}

	public function insert_group( $args ){
		global $wpdb, $wp_query;

		$trainer_id = urldecode($wp_query->query_vars['trainor_id']);
		$groupMembers =  urldecode($wp_query->query_vars['groupMembers']);
		$name= urldecode($wp_query->query_vars['name']);
		$details =  urldecode($wp_query->query_vars['details']);
	
		$query = "INSERT INTO {$wpdb->prefix}groups (name,details,groupMembers,trainer_id) VALUES ('$name','$details','$groupMembers','$trainer_id')";
		$query = $wpdb->query( $query );
		$group_id = $wpdb->insert_id;

		$response['results'] = $group_id;

		return $response;
	}

	public function update_group( $args ){
		global $wpdb, $wp_query;

		$group_id = urldecode($wp_query->query_vars['id']);
		$groupMembers =  urldecode($wp_query->query_vars['groupMembers']);
		$name= urldecode($wp_query->query_vars['name']);
		$details =  urldecode($wp_query->query_vars['details']);
	
		$query = "UPDATE {$wpdb->prefix}groups SET name='$name',details='$details',groupMembers='$groupMembers' WHERE id=$group_id";
		$query = $wpdb->query( $query );


		$response['results'] = $query;

		return $response;
	}

	public function get_last_message( $args ){
		global $wpdb, $wp_query;
		$conversation_id =  $wp_query->query_vars['id'];
		$response['results']['messages'] = "This API required UM Messaging API";
		if ( class_exists( 'UM_Messaging_API') ) {
			$query = "SELECT * FROM {$wpdb->prefix}um_messages WHERE conversation_id = '$conversation_id' ORDER BY time DESC LIMIT 1";
			$conversations = $wpdb->get_results( $query );
			$response['results']['messages'] = $conversations;
		}
		return $response;
	}

	public function get_last_weight_goal( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];
			$query2 = "SELECT id FROM {$wpdb->prefix}weight_goal WHERE user_id = '$user_id' ORDER BY id
			 DESC LIMIT 1";
		$results2 = $wpdb->get_results( $query2 );
		$something = $results2[0]->id;
			$query = "SELECT * FROM {$wpdb->prefix}weight_goal INNER JOIN {$wpdb->prefix}weight_progress ON 
			{$wpdb->prefix}weight_goal.id={$wpdb->prefix}weight_progress.goal_id  WHERE goal_id = '$something' ORDER BY id
			 ";
		if($wpdb->get_results( $query )){
			$results = $wpdb->get_results( $query );
			$response['results'] = $results;
		}else{
			$query = "SELECT * FROM {$wpdb->prefix}weight_goal WHERE user_id = '$user_id' ORDER BY id
			DESC LIMIT 1";
			$results = $wpdb->get_results( $query );
			$response['results'] = $results;
		}	 
		return $response;
	}

	public function insert_message( $args ){
		global $wpdb, $wp_query;
		$message_content = $wp_query->query_vars['message_content'];
		$conversation_id =  $wp_query->query_vars['id'];
		$recipient =  $wp_query->query_vars['recipient'];
		$author =  $wp_query->query_vars['author'];

		$response['results'] = "This API required UM Messaging API";
		if ( class_exists( 'UM_Messaging_API') ) {
			$query = "INSERT INTO {$wpdb->prefix}um_messages (conversation_id,time,content,status,author,recipient) VALUES ('$conversation_id',NOW(),'$message_content',0,'$author','$recipient')";
			$query = $wpdb->query( $query );
			$message_id = $wpdb->insert_id;

			$query = "UPDATE {$wpdb->prefix}um_conversations SET last_updated=NOW() WHERE conversation_id='$conversation_id'";
			$query = $wpdb->query( $query );

			$response['results'] = $query;
		}
		return $response;
	}

	public function get_recent_messages( $args ){
		global $wpdb, $wp_query, $ultimatemember;
		$user_id =  $wp_query->query_vars['id'];

		$query = "SELECT `con`.`conversation_id`, `con`.`last_updated`, `mess`.* FROM `fcWP_um_conversations` `con` INNER JOIN (SELECT * FROM `fcWP_um_messages` group by `conversation_id`) `mess` ON `con`.`conversation_id` = `mess`.`conversation_id` WHERE `mess`.`recipient` = '$user_id' OR `mess`.`author` = '$user_id' LIMIT 3";

		$results = $wpdb->get_results( $query );
		foreach ($results as $result) {
			$user_id = $result->author == $user_id ? $result->recipient : $result->author;
			um_fetch_user($user_id);

			$result->user_name = um_profile('display_name');
			$result->user_profile_pic = $this->getsrc( um_user('profile_photo', 200) );

		}
		$response['results'] = $results;

		return $response;

	}

	/*************************
	 * Mailchimp api request *
	 *************************/

	public function get_interest_categories( $args ){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, "https://us16.api.mailchimp.com/3.0/lists/7885d69751/interest-categories/edc83b1259/interests");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		curl_setopt($ch, CURLOPT_USERPWD, "anystring" . ":" . "b3b7a3d60765ef25388ee3dc4f3c0004-us16");

		$result = curl_exec($ch);
		$result = json_decode($result);
		if (curl_errno($ch)) {
		    echo 'Error:' . curl_error($ch);
		}
		curl_close ($ch);

		return $result;
	}

	/*******************
	 * Calendar Datas  *
	 *******************/
	public function get_fitness_data_for_calendar_widget( $args ){
		global $wpdb, $wp_query;

		$user_id =  $wp_query->query_vars['id'];
		$query = "SELECT * FROM `fcWP_fitness_datas` WHERE `created_to` = '$user_id' GROUP BY `date` ";
		$fitness_data = $wpdb->get_results( $query );
		$response['results'] = $fitness_data;

		return $response;
	}

	/***
	Fitness logs
	*****/
	public function getFitnesslogs( $args ){
		global $wpdb, $wp_query;
		$trainer_id = $wp_query->query_vars['id'];
		$client_id = $wp_query->query_vars['client_id'];

		$query = "SELECT * FROM `fcWP_fitness_logs` WHERE `created_by` = '$trainer_id' AND `created_to` = '$client_id'";
		$fitness_data = $wpdb->get_results( $query );
		$response['results'] = $fitness_data;

		return $response;

	}

	/************
	 * Program  *
	 ************/
	public function get_programs( $args ){
		global $wpdb, $wp_query;

		$user_id =  $wp_query->query_vars['id'];
		$query = "SELECT * FROM `fcWP_programs` WHERE `owner_id` = '$user_id'";
		$programs_data = $wpdb->get_results( $query );
		$response['results'] = $programs_data;

		return $response;
	}

	public function favorite_program( $args ){
		global $wpdb, $wp_query;

		$program_id =  $wp_query->query_vars['id'];
		$status = $wp_query->query_vars['status'];

		$query = "UPDATE `fcWP_programs` SET `favorite` = '$status' WHERE `id` = '$program_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;
	}

	public function delete_program( $args ){
		global $wpdb, $wp_query;
		$programs_id =  $wp_query->query_vars['id'];

		$query = "DELETE FROM `fcWP_programs` WHERE `id` = '$programs_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;
	}

	public function get_program_lits( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];

		$query = "SELECT `fcWP_programs`.`id`, `fcWP_programs`.`name`, `fcWP_programs`.`instructions`, `fcWP_programs`.`category`, `fcWP_programs`.`video_link`, `fcWP_programs`.`thumbnail_url`, `fcWP_programs`.`favorite`, GROUP_CONCAT(`fcWP_program_sets`.`repetition`) AS 'REPS', GROUP_CONCAT(`fcWP_program_sets`.`loads`) AS 'LOADS', GROUP_CONCAT(`fcWP_program_sets`.`rests`) AS 'RESTS', GROUP_CONCAT(`fcWP_program_sets`.`set_no`) AS 'SETS' FROM `fcWP_programs` INNER JOIN `fcWP_program_sets` ON `fcWP_programs`.id = `fcWP_program_sets`.program_id where `owner_id` = '$user_id' GROUP BY `fcWP_program_sets`.`program_id`";

		$results = $wpdb->get_results( $query );
		$response['results'] = $results;

		return $response;

	}

	public function add_program( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];
		$program = urldecode($wp_query->query_vars['program']);

		$program = stripcslashes ($program);
		$program = json_decode($program);

		$name = $program->name;
		$tags = $program->tags;
		$difficulty = $program->difficulty;
		$description = $program->description;
		$instructions = $program->instrution;
		$notes = $program->notes;
		$video_link = $program->video;
		$thumbnail_url = $program->thumbnail;
		$favorite = 0;

		$query = "INSERT INTO `fcWP_programs` (`id`, `name`, `descriptions`, `instructions`, `difficulty`, `notes`, `video_link`, `thumbnail_url`, `owner_id`, `tags`, `favorite`) VALUES (NULL, '$name', '$description', '$instructions','$difficulty','$notes', '$video_link', '$thumbnail_url', '$user_id', '$tags', '$favorite')";
		$result = $wpdb->query( $query );
		$program_id = $wpdb->insert_id;

		$sets = $program->sets;
		$counter = 1;
		foreach ($sets as $set) {
			$set_no = $counter;
			$rep = $set->rep;
			$load = $set->load;
			$rest = $set->rest;

			$query2 = "INSERT INTO `fcWP_program_sets` (`id`, `program_id`, `repetition`, `loads`, `rests`, `set_no`) VALUES (NULL, '$program_id', '$rep', '$load', '$rest', '$set_no')";

			$result2 = $wpdb->query( $query2 );

			$counter+=1;
		}

		
		//var_dump($program);
		return $result2;
	}

	/************
	 * Package  *
	 ************/
	public function getFullPackagesDetail($args){
		global $wpdb, $wp_query;

		$user_id = $wp_query->query_vars['id'];
		$query = "SELECT * FROM `fcWP_packages` inner join `fcWP_trial_package_week` On fcWP_trial_package_week.trial_package_id=fcWP_packages.id where fcWP_packages.id=$user_id";
		$package_data = $wpdb->get_results( $query );
		$response['week_plan'] = $package_data;

	foreach($response['week_plan'] as $row=>$value){

		$something =(int)$value->trial_week_plan_id;


		$query = "SELECT * FROM `fcWP_trial_nutrition` inner join `fcWP_trial_package_week` on fcWP_trial_nutrition.trial_week_plan_id=fcWP_trial_package_week.trial_week_plan_id where fcWP_trial_nutrition.trial_week_plan_id=$something";

		$response['week_plan'][$row]->nutrition =$wpdb->get_results( $query );
		
	    $query = "SELECT * FROM `fcWP_trial_exercise` inner join `fcWP_trial_package_week` on fcWP_trial_exercise.trial_week_plan_id=fcWP_trial_package_week.trial_week_plan_id where fcWP_trial_exercise.trial_week_plan_id=$something";

		$response['week_plan'][$row]->exercises =$wpdb->get_results( $query );
		
	   $query = "SELECT * FROM `fcWP_trial_workout` inner join `fcWP_trial_package_week` on fcWP_trial_workout.trial_week_plan_id=fcWP_trial_package_week.trial_week_plan_id where fcWP_trial_workout.trial_week_plan_id=$something";

		$response['week_plan'][$row]->workout =$wpdb->get_results( $query );
		foreach($response['week_plan'][$row]->workout as $row2=>$value2){
		$counter =  (int)$value2->trial_workout_id;
		   $query = "SELECT * FROM `fcWP_trial_workout_detail` inner join `fcWP_trial_workout` on fcWP_trial_workout_detail.trial_workout_id=fcWP_trial_workout.trial_workout_id where fcWP_trial_workout_detail.trial_workout_id=1";
		   	$response['week_plan'][$row]->workout[$row2]->exercises =
		   	$wpdb->get_results( $query ); 
		}
	 }
		// return $something;
		return $response;
	}


	public function insert_fullPackage(){
	global $wpdb, $wp_query;	
	$package_name= $wp_query->query_vars['name'];
	$package_tags= $wp_query->query_vars['tags'];	
	$package_descriptions= $wp_query->query_vars['description'];	
	$owner_id = $wp_query->query_vars['id'];
	$dataArray = stripslashes($wp_query->query_vars['week_plan']);	
	$data = json_decode($dataArray);
	 $query = "INSERT into `fcWP_packages` (name,descriptions,tags,owner_id) VALUES('$package_name','$package_descriptions',
	 '$package_tags','$owner_id')";

	 $query = $wpdb->query($query);
	 $super = $wpdb->insert_id;

	foreach($data as $row=>$value){
	$week_no = $row+1;	

	 $query = "INSERT into `fcWP_trial_package_week` (trial_package_id,week_no) VALUES('$super','$week_no')";
	 $query = $wpdb->query($query);
	 $week_id = $wpdb->insert_id;
		foreach($data[$row]->exercises as $row2=>$value2){	
		 $name = $value2->name;
		 $day_no = $value2->day_no;	
		 $query = "INSERT into `fcWP_trial_exercise` (trial_week_plan_id,name,day_no) VALUES('$week_id','$name','$day_no')";
		  $query = $wpdb->query($query);
		}
		foreach($data[$row]->workout as $row2=>$value2){	
		 $name = $value2->name;
		 $day_no = $value2->day_no;	
		 $query = "INSERT into `fcWP_trial_workout` (trial_week_plan_id,name,day_no) VALUES('$week_id','$name','$day_no')";
		  $query = $wpdb->query($query);
		}	
		foreach($data[$row]->nutrition as $row2=>$value2){	
		 $name = $value2->name;
		 $day_no = $value2->day_no;
		 $calories = $value2->calories;	
		 $query = "INSERT into `fcWP_trial_nutrition` (trial_week_plan_id,food_name,day_no,calories) VALUES('$week_id','$name','$day_no','$calories')";
		  $query = $wpdb->query($query);
		}	
	}	   
	 $response['success'] = $super; 
	 return $data;
	}

	public function get_workouts(){
		global $wpdb, $wp_query;
		$id =  $wp_query->query_vars['id'];
		$action =  $wp_query->query_vars['action'];
	    $name = preg_replace('/(?<!\s)-(?!\s)/', ' ', $wp_query->query_vars['name']);
	    $noOfWeeks =  $wp_query->query_vars['no_weeks'];
	    $notes = $wp_query->query_vars['notes'];
	    $exercises = $wp_query->query_vars['exercises'];
		if($id==null||$id==''){
		$query = "SELECT * FROM `fcWP_workout`";
		$response =  $wpdb->get_results( $query );
		}else{
			if($action ==null||$action == ''){
				$query = "SELECT * FROM `fcWP_workout` WHERE `id`='$id'";
				$response =  $wpdb->get_results( $query );
			}else if($action == 'edit'){
		 		 $query = "UPDATE `fcWP_workout`SET name='$name',no_of_weeks='$noOfWeeks',notes='$notes',exercises='$exercises' WHERE  `id`='$id'";
		  		 $response = $wpdb->query($query);
			}else if($action == 'delete'){
		 		 $query = "DELETE from `fcWP_workout` WHERE  `id`='$id'";
				  $response = $wpdb->query($query);
			}

		}

		return $response;
	}

	public function get_allFood(){
		global $wpdb, $wp_query;
		$query = "SELECT * FROM `fcWP_food`";
		$response =  $wpdb->get_results( $query );

		return $response;
	}

	public function get_packages( $args ){
		global $wpdb, $wp_query;

		$user_id =  $wp_query->query_vars['id'];
		$query = "SELECT * FROM `fcWP_packages` WHERE `owner_id` = '$user_id'";
		$package_data = $wpdb->get_results( $query );
		$response['results'] = $package_data;

		return $response;
	}


	public function favorite_package( $args ){
		global $wpdb, $wp_query;
		$package_id =  $wp_query->query_vars['id'];
		$status = $wp_query->query_vars['status'];

		$query = "UPDATE `fcWP_packages` SET `favorite` = '$status' WHERE `id` = '$package_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;

	}

	public function delete_package( $args ){
		global $wpdb, $wp_query;
		$package_id =  $wp_query->query_vars['id'];

		$query = "DELETE FROM `fcWP_packages` WHERE `id` = '$package_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;
	}

	public function add_package( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];
		$package =  $wp_query->query_vars['data'];

		$package = stripcslashes ($program);
		$package = json_decode($program);

		return $package;
	}

	public function get_package_details( $args ){
		global $wpdb, $wp_query;
		$package_id =  $wp_query->query_vars['id'];

		$query_package_details = "SELECT * from `fcWP_packages` where `id` = '$package_id'";
		$package_details = $wpdb->get_results( $query_package_details );

		$query_package_media = "SELECT * FROM `fcWP_package_medias` where `package_id` = '$package_id'";
		$package_media = $wpdb->get_results( $query_package_media );

		$query_package_weekplans = "SELECT * FROM `fcWP_package_week_plans` where `package_id` = '$package_id'";
		$package_weekplans = $wpdb->get_results( $query_package_weekplans );

		$result['id'] = $package_id;
		$result['package_details'] = $package_details;
		$result['package_media'] = $package_media;
		$result['package_weekplans'] = $package_weekplans;
		$program = $result->package_weekplans->program;
		$result->package_weekplans->program = json_decode($program);

		return $result;
	}

	public function changeStatus( $args ){
		global $wpdb, $wp_query;
		$package_id =  $wp_query->query_vars['id'];
		$status = $wp_query->query_vars['status'];

		$query = "UPDATE `fcWP_packages` SET `active` = '$status' WHERE `id` = '$package_id'";
		$query = $wpdb->query( $query );
		$response['results'] = $query;

		return $response;
	}

	public function getClientPackage( $args ){
		global $wpdb, $wp_query;
		$user_id =  $wp_query->query_vars['id'];
		$group_id = $wp_query->query_vars['group_id'];

		$group_id = stripcslashes ($group_id);
		$group_id = json_decode($group_id);
		$gIDs = implode(',', $group_id);

		$query = "SELECT GROUP_CONCAT(`package_id`) as `packages` FROM `fcWP_packages_members` WHERE (`type` = 'group' AND `member` IN ('$gIDs')) OR (`type` = 'client' AND `member` = '$user_id')";
		$packages = $wpdb->get_results( $query );

		$package_ids = $packages[0]->packages;

		$query_package_lists = "SELECT * FROM `fcWP_packages` WHERE `id` IN ($package_ids) AND `active` = '1'";
		$packages_lists = $wpdb->get_results( $query_package_lists );

		return $packages_lists;

	}

	/*************************
	 * User Events functions *
	 *************************/
	public function user_register_to_Event( $args ){
		global $wpdb, $wp_query;

		$event_id = $wp_query->query_vars['event_id'];
		$event_name = $wp_query->query_vars['event_name'];
		$event_start_date = $wp_query->query_vars['event_start_date'];
		$event_end_date = $wp_query->query_vars['event_end_date'];
		$event_location = $wp_query->query_vars['event_location'];
		$user_id = $wp_query->query_vars['id'];

		//$regisration_date = $wp_query->query_vars['regisration_date'];
		$attd_id = 0;
		$txn_id = 0;
		$ticket_id = 0;
		$reg_price = 0;
		$reg_paid = 0;
		$reg_session = hash('md5', rand());

		$query = "INSERT INTO fcWP_esp_registration (`REG_ID`, `EVT_ID`, `ATT_ID`, `TXN_ID`, `TKT_ID`, `STS_ID`, `REG_date`, `REG_final_price`, `REG_paid`, `REG_session`, `REG_code`, `REG_url_link`, `REG_count`, `REG_group_size`, `REG_att_is_going`, `REG_deleted`) VALUES (NULL, '$event_id', '0', '0', '0', 'RPP', NOW(), '0.000', '0.000', '$reg_session', NULL, NULL, '1', '1', '0', '0')";
		$query = $wpdb->query($query);
		$registration_id = $wpdb->insert_id;

		$query_two = "INSERT INTO `fcWP_users_events` (`id`, `event_id`, `registration_id`, `event_name`, `event_location`, `user_id`, `reg_date`, `event_start_date`, `event_end_date`) VALUES (NULL, '$event_id', '$registration_id', '$event_name', '$event_location', '$user_id', NOW(), '$event_start_date', '$event_end_date')";

		$query_two = $wpdb->query($query_two);
		$user_events_id = $wpdb->insert_id;

		
		$response['registration_id'] = $registration_id;
		$response['user_events_id'] = $user_events_id;
		$response['reg_session'] = $reg_session;

		return $response;
	}

	public function get_user_events( $args ){
		global $wpdb, $wp_query;
		$user_id = $wp_query->query_vars['id'];

		$query = "SELECT * FROM {$wpdb->prefix}users_events WHERE user_id = '$user_id'";
		$events = $wpdb->get_results( $query );
		$response['results'] = $events;

		return $response;
	}

	public function get_upcoming_events( $args ){
		global $wpdb, $wp_query;
		$user_id = $wp_query->query_vars['id'];

		$query = "SELECT * FROM `fcWP_users_events` WHERE `event_start_date` >= CURDATE() AND `user_id` = '$user_id' ORDER BY `event_start_date`";

		$events = $wpdb->get_results( $query );
		$response['results'] = $events;

		return $response;
	}

	public function get_events_bydate( $args ){
		global $wpdb, $wp_query;
		$event_date = $wp_query->query_vars['event_start_date'];
		$user_id = $wp_query->query_vars['id'];

		$query = "SELECT * FROM `fcWP_users_events` WHERE `event_start_date` = '$event_date' AND `user_id` = '$user_id'";
		$events = $wpdb->get_results( $query );
		$response['results'] = $events;

		return $response;
	}

	/**
	 * Start of New API Adjustment
	 *
	 */

		//Module Name: Users
		    //Registration 
	public function register_user_demo(){
		global $wpdb, $wp_query;
		$fname = $wp_query->query_vars['fname'];
		$email = $wp_query->query_vars['email'];
		$country = $wp_query->query_vars['country'];
		$companyName = $wp_query->query_vars['companyName'];
		$phone = $wp_query->query_vars['phone'];
		$industry = $wp_query->query_vars['industry'];
		$lname = $wp_query->query_vars['lname'];
		$role = $wp_query->query_vars['role'];
 
	$query = "INSERT INTO fcWP_demo_reservation (first_name,last_name,user_email,country,company_name,phone_number,industry,role) VALUES ('$fname','$lname','$email','$country','$companyName','$phone','$industry','$role')";
	$query = $wpdb->query($query);
	$registration_id = $wpdb->insert_id;


	$registration_id = $wpdb->insert_id;
	return $registration_id;
	}

	public function register_user(){
		global $wpdb, $wp_query;
		$fname = $wp_query->query_vars['fname'];
		$lname = $wp_query->query_vars['lname'];
		$country = $wp_query->query_vars['country'];
		$companyName = $wp_query->query_vars['companyName'];
		$phone = $wp_query->query_vars['phone'];
		$industry = $wp_query->query_vars['industry'];
		$display_name = $fname  . " " . $lname;
		$user_login = $wp_query->query_vars['email'];
		$email = $wp_query->query_vars['email'];
		$user_pass =  wp_hash_password($this->gen_random_str(12));
		$role = $wp_query->query_vars['role'];
		$community_role = $wp_query->query_vars['community_role'];
		$user['member-registration-trainer-vendor'] = $wp_query->query_vars['vendor'];
		$activation_link =  wp_hash_password($this->gen_random_str(12));
		$activation_url = urlencode($activation_link);
		$query = "INSERT INTO fcWP_users (user_login,user_pass,user_nicename,user_email,user_activation_key) VALUES ('$user_login','$user_pass','$fname','$email','$activation_link')";
			
		$query = $wpdb->query($query);
		$new_id = $wpdb->insert_id;


		$query = "INSERT INTO fcWP_user_details (user_id,first_name,last_name,country,company_name,phone_number,industry,community_role) VALUES ('$new_id','$fname','$lname','$country','$companyName','$phone','$industry','$role')";
		$query = $wpdb->query($query);
	
	
		$to = $email;
		$subject = "Verify Fitcode Account";
		$body = "<html> <body>Hi, This is an email to verify your fitcode account! This is your activation_link code  <a href='http://localhost:4200/register-verification/" . $activation_url . "/" . $new_id  ."'>" . $activation_link . "</a></body> </html>";
		// $headers[] = "From: Fitcode <no-reply@fitcode.com>";
		$headers = array('Content-Type: text/html; charset=UTF-8');
		$attachments ="";
		return wp_mail($to,$subject,stripslashes($body),$headers);
			}

	public function gen_random_str($length, $keyspace = '!?0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'){
    $str = '';

    $max = mb_strlen($keyspace, '8bit') - 1;
    for ($i = 0; $i < $length; ++$i) {
        $str .= $keyspace[random_int(0, $max)];
    }
    return $str;
}	

	public function check_validation_status(){
		global $wpdb, $wp_query;
  		$userID = $wp_query->query_vars['id'];
  		$password =urldecode($wp_query->query_vars['password']);
  		$query = "SELECT * FROM `fcWP_users` WHERE `ID` = '$userID' AND 
  		`user_activation_key` = '$password' AND `user_status`=0";
	$response['user'] = $wpdb->get_results( $query );
		if(!empty($response['user'])){
			$result = "Success";
		}else{
			$result = "Invalid";
		}

		return $result;
	}

  	public function insert_initial_password(){
  		global $wpdb, $wp_query;
  		$userID = $wp_query->query_vars['id'];
  		$password =wp_hash_password($wp_query->query_vars['password']);
  		$query ="UPDATE `fcWP_users` set user_pass='$password',user_status=1 where ID='$userID'";
  		$query = $wpdb->query($query);
		$registration_id = $wpdb->insert_id;
		return "true";
  	}	

	public function check_user_exist(){
		global $wpdb, $wp_query;
		$userEmail = $wp_query->query_vars['email'];
		$query = "SELECT * FROM `fcWP_users` WHERE `user_login` = '$userEmail'";
		$response['user'] = $wpdb->get_results( $query );
		$query = "SELECT * FROM `fcWP_demo_reservation` WHERE `user_email` = '$userEmail'";
		$response['demo'] = $wpdb->get_results( $query );

		if(!empty($response['user'])){
			$result = "Existing Account";
			if($response['user']->user_status==0){
				$result = "Inactivated Account";
			}
		}else if(!empty($response['demo'])){
			$result = "For Demo Account";
		}else{
			$result = "Account not yet exist";
		}
		return $result;
	}		


			//End of Registration
	
    	    //Start of Workout 

	public function insert_workout(){
	  global $wpdb, $wp_query;
	  $name = preg_replace('/(?<!\s)-(?!\s)/', ' ', $wp_query->query_vars['name']);
	  $noOfWeeks =  $wp_query->query_vars['no_weeks'];
	  $notes = $wp_query->query_vars['notes'];
	  $exercises = $wp_query->query_vars['exercises'];

	  $query = "INSERT into `fcWP_workout`(name,no_of_weeks,notes,exercises,category) VALUES('$name','$noOfWeeks','$notes','$exercises','workout')";
	    $query = $wpdb->query($query);
		$registration_id = $wpdb->insert_id;
		return $notes;
	}
			//End of Workout
	//Clients
	public function get_allClients(){
		global $wpdb, $wp_query;
		$trainer_id = $wp_query->query_vars['trainer_id'];
		$query = "SELECT * FROM fcWP_user_trainers trainer INNER JOIN (SELECT ID, online_status, user_email, last_online FROM fcWP_users) user ON trainer.user_id = user.ID INNER JOIN (SELECT user_id, first_name, last_name, address, birthday, height, weight, phone_number FROM fcWP_user_details) details on user.ID = details.user_id WHERE trainer.trainer_id = $trainer_id";
		$results = $wpdb->get_results( $query );
		return $results;
	}

	public function add_client(){
		global $wpdb, $wp_query;
		$first_name = $wp_query->query_vars['fname'];
		$last_name = $wp_query->query_vars['lname'];
		$display_name = $first_name . " " . $last_name;
		$email = $wp_query->query_vars['email'];
		$user_pass =  wp_hash_password($this->gen_random_str(12));
		$gender = $wp_query->query_vars['gender'];
		$group_ids = $wp_query->query_vars['groupids'];
		$user_type = $wp_query->query_vars['user_type'];
		$trainer_id = $wp_query->query_vars['trainer_id'];
		$activation_link =  wp_hash_password($this->gen_random_str(12));
		$activation_url = urlencode($activation_link);

		$query = "INSERT INTO fcWP_users (user_login,user_nicename,user_email,user_activation_key,display_name,user_type,user_pass) VALUES ('$email','$fname','$email','$activation_link','$display_name','$user_type','$user_pass')";

		$query = $wpdb->query($query);
		$new_id = $wpdb->insert_id;

		if($query == 1){		// if query was successful
			$query2 = "INSERT INTO fcWP_user_trainers (user_id,trainer_id) VALUES ('$new_id','$trainer_id')";
			$query2 = $wpdb->query($query2);

			if($query2 == 1){	// if query2 was successful
				$query3 = "INSERT INTO fcWP_user_details (user_id,first_name,last_name,gender,community_role) VALUES ('$new_id','$first_name','$last_name','$gender','$user_type')";

				$query3 = $wpdb->query($query3);
				
				if($query3 == 1){	// if this last query was successful
					$to = $email;
					$subject = "Verify Fitcode Account";
					$body = "<html> <body>Hi, This is an email to verify your fitcode account! This is your activation_link code  <a href='http://localhost:4200/register-verification/" . $activation_url . "/" . $new_id  ."'>" . $activation_link . "</a></body> </html>";
					// $headers[] = "From: Fitcode <no-reply@fitcode.com>";
					$headers = array('Content-Type: text/html; charset=UTF-8');
					$attachments ="";
					return wp_mail($to,$subject,stripslashes($body),$headers);
				}
				else{
					return "false : query to user details unsuccessful";
				}
			}
			else{
				return "false : query to user details unsuccessful";
			}
		}
		else{
			return "false : query to user trainers unsuccessful";
		}
	}
		//End of Clients
		//End of Users module
		//Module Name: fitness_logs
			//End of fetch user details
		public function fetch_user_details(){
		global $wpdb, $wp_query;
		$userID = $wp_query->query_vars['id'];
		$query = "SELECT * FROM `fcWP_users` inner join `fcWP_user_details` on fcWP_users.id = fcWP_user_details.user_id WHERE fcWP_users.id = '$userID'";
		$result =  $wpdb->get_results( $query );

		return $result[0];
		}

			//End of fetch user details	
		//End of Users module


		//Module Name: fitness_logs

		//Trainer Home page
		public function get_allfitnesslogs( $args ){
			global $wpdb, $wp_query;
			$user_id = $wp_query->query_vars['id'];

			$query = "SELECT * FROM `fcWP_fitness_logs` WHERE `created_to` = '$user_id'";
			$user_fitness = $wpdb->get_results( $query );
			$response['results'] = $user_fitness;

			return $response;
		}

		public function add_fitnesslogs( $args ){
			global $wpdb, $wp_query;
			$created_to = $wp_query->query_vars['created_to'];
			$created_by = $wp_query->query_vars['created_by'];
			$date = $wp_query->query_vars['date'];
			$type = $wp_query->query_vars['type'];
			$datas = $wp_query->query_vars['datas'];

			$query = "INSERT INTO `fcWP_fitness_logs` (`id`, `created_to`, `created_by`, `log_date`, `status`, `notif_status`, `type`, `datas`) VALUES (NULL, '$created_to', '$created_by', '$date', '0', '0', '$type', '$datas')";
			$fitnesslogs = $wpdb->get_results( $query );
			if($fitnesslogs){
				return true;
			}else{
				return false;
			}
		}

		public function get_logsCalWidget( $args ){
			global $wpdb, $wp_query;
			$user_id = $wp_query->query_vars['id'];

			$query = "SELECT * FROM `fcWP_fitness_logs` WHERE `created_to` = '$user_id' GROUP BY `log_date` ";
			$fitness_logs = $wpdb->get_results( $query );
			$response['results'] = $fitness_logs;

			return $response;
		}

		//End of fitness_logs module


		//Module Name: Coaching
		public function get_client_reports( $args ){
			global $wpdb, $wp_query;
			$trainer_id = $wp_query->query_vars['trainer_id'];

			$query = "SELECT * FROM fcWP_user_trainers trainer INNER JOIN (SELECT ID, online_status, user_email, last_online FROM fcWP_users) user ON trainer.user_id = user.ID INNER JOIN (SELECT user_id, first_name, last_name, address, birthday, height, weight, phone_number FROM fcWP_user_details) details on user.ID = details.user_id WHERE trainer.trainer_id = $trainer_id";
			$clients = $wpdb->get_results( $query );

			foreach($clients as $client){
				$client_id = $client->ID;
				$query = "SELECT * FROM `fcWP_fitness_logs` WHERE `created_to` = '$client_id'";
				$logs = $wpdb->get_results( $query );
				$client->fitness_logs = $logs;
			}

			return $clients;



		}
		//End of Trainer Home
		//End of Coaching module


	/**
	 * END of New API Adjustment
	 *
	 */

	/**
	 * Displays a missing authentication error if all the parameters aren't
	 * provided
	 */
	private function missing_auth() {
		$error = array();
		$error['error'] = __( 'You must specify both a token and API key!', 'ultimatemember' );

		$this->data = $error;
		$this->output( 401 );
	}

	/**
	 * Displays an authentication failed error if the user failed to provide valid credentials
	 */
	private function invalid_auth() {
		$error = array();
		$error['error'] = __( 'Your request could not be authenticated', 'ultimatemember' );

		$this->data = $error;
		$this->output( 401 );
	}

	/**
	 * Displays an invalid API key error if the API key provided couldn't be validated
	 */
	private function invalid_key() {
		$error = array();
		$error['error'] = __( 'Invalid API key', 'ultimatemember' );

		$this->data = $error;
		$this->output( 401 );
	}


	/**
	 * Listens for the API and then processes the API requests
	 */
	public function process_query() {
		global $wp_query;

		// Check for um-api var. Get out if not present
		if ( ! isset( $wp_query->query_vars['um-api'] ) )
			return;

		// Check for a valid user and set errors if necessary
		$this->validate_request();

		// Only proceed if no errors have been noted
		if( ! $this->is_valid_request )
			return;

		if( ! defined( 'UM_DOING_API' ) ) {
			define( 'UM_DOING_API', true );
		}

		// Determine the kind of query
		$query_mode = $this->get_query_mode();
		foreach( $this->vars as $k ) {
			$args[ $k ] = isset( $wp_query->query_vars[ $k ] ) ? $wp_query->query_vars[ $k ] : null;
		}

		$data = array();

		switch( $query_mode ) :

			case 'get.stats':
				$data = $this->get_stats( $args );
				break;
				
			case 'get.users':
				$data = $this->get_users( $args );
				break;
				
			case 'get.user':
				$data = $this->get_auser( $args );
				break;
				
			case 'update.user':
				$data = $this->update_user( $args );
				break;
				
			case 'delete.user':
				$data = $this->delete_user( $args );
				break;

			case 'get.following':
				$data = $this->get_following( $args );
				break;
				
			case 'get.followers':
				$data = $this->get_followers( $args );
				break;

			case 'is.user':
				$data = $this->is_user( $args );
				break;

			case 'reset.password':
				$data = $this->reset_password( $args );
				break;

			case 'change.password':
				$data = $this->change_password( $args );
				break;

			case 'register.user':
				$data = $this->register_user( $args );
				break;

			case 'update.profile':
				$data = $this->update_profile( $args );
				break;

			case 'add.members':
				$data = $this->add_members( $args );
				break;

			case 'get.members':
				//$data = $this->get_members( $args );
				$data = $this->get_all_clients( $args );
				break;
			case 'get.trainers':
				//$data = $this->get_members( $args );
				$data = $this->get_all_trainers( $args );
				break;	
			case 'get.conversation':
				$data = $this->get_conversation( $args );
				break;

			case 'get.message':
				$data = $this->get_message( $args );
				break;
			case 'get.usersByRole':
				$data = $this->get_users_by_role( $args);	
				break;		
			case 'add.group':
				$data = $this->insert_group($args);
				break;					
			case 'update.group':
				$data = $this->update_group($args);
				break;		
			case 'user.registerToEvent':
				$data = $this->user_register_to_Event( $args );
				break;	
			case 'get.interestCategories':
				$data = $this->get_interest_categories( $args );
				break;
			case 'addconversation.group':
				$data = $this->insert_conversation_to_group( $args );
				break;
			case 'addmessage.group':
				$data = $this->insert_message_to_group( $args );
				break;
			case 'get.userEvents':
				$data = $this->get_user_events( $args );
				break;
			case 'getconversation.group':
				$data = $this->get_group_conversation( $args );
				break;
			case 'getmessage.group':
				$data = $this->get_group_message( $args );
				break;
			case 'get.last.message.group':
				$data = $this->get_last_group_message( $args );
				break;
			case 'get.clients':
				$data = $this->get_all_clients( $args );
				break;
			case 'accept.client';
				$data = $this->acceptAsClient( $args );
				break;
			case 'fitness.data.calwidget':
				$data = $this->get_fitness_data_for_calendar_widget( $args );
				break;
			case 'get.upcomingEvents':
				$data = $this->get_upcoming_events( $args );
				break;
			case 'get.groups':
				$data = $this->getYourGroups( $args );
				break;
			case 'get.programs':
				$data = $this->get_programs( $args );
				break;
			case 'get.last.weight':
				$data = $this->get_last_weight_goal( $args );
				break;
			case 'get.packages':
				$data = $this->get_packages( $args );
				break;
			case 'favorite.package':
				$data = $this->favorite_package( $args );
				break;
			case 'delete.package':
				$data = $this->delete_package( $args );
				break;
			case 'get.fullPackages':
				$data = $this->getFullPackagesDetail($args);
				break;
			case 'favorite.program':
				$data = $this->favorite_program( $args );
				break;
			case 'delete.program':
				$data = $this->delete_program( $args );
				break;
			case 'get.eventsByDate':
				$data = $this->get_events_bydate( $args );
				break;
			case 'get.programLists':
				$data = $this->get_program_lits( $args );
				break;
			case 'favorite.group':
				$data = $this->favorite_group( $args );
				break;
			case 'get.recentMessages':
				$data = $this->get_recent_messages( $args );
				break;
			case 'get.userDetails':
				$data = $this->get_user_details( $args );
				break;
			case 'add.program':
				$data = $this->add_program( $args );
				break;
			case 'add.package':
				$data = $this->add_package( $args );
				break;
			case 'get.packageDetails':
				$data = $this->get_package_details( $args );
				break;
			case 'change.status':
				$data = $this->changeStatus( $args );
				break;
			case 'get.workout':
				$data = $this->get_workouts( $args );
				break;			
			case 'get.clientPackage':
				$data = $this->getClientPackage( $args );
			case 'get.allFood':
				$data = $this->get_allFood( $args );	
				break;
			case 'get.fitnesslogs':
				$data = $this->getFitnesslogs( $args );
				break;
			case 'insert.fullPackage':
				$data = $this->insert_fullPackage( $args );
				break;

		// New API for Adjustment

			// Module_Name : USERS
				//Registration
			case 'check.user.exist':
				$data = $this->check_user_exist( $args );
				break;
			case 'register.user.demo':
				$data = $this->register_user_demo($args);
				break;	
			case 'insert.initial.password':
				$data = $this->insert_initial_password($args);
				break;	
			case 'check.validation.status':
				$data = $this->check_validation_status($args);
				break;
				//
				//Fetch Users
			case 'fetch.user.details':
				$data = $this->fetch_user_details($args);
				break;
			case 'get.allClients':
				$data = $this->get_allClients( $args );
				break;
			case 'add.client':
				$data = $this->add_client( $args );
				break;

				//
			//End of Users Module


			// Module_Name : fitness_logs
			case 'get.allFitnesslogs':
				$data = $this->get_allfitnesslogs( $args );
				break;

			case 'add.fitnesslogs':
				$data = $this->add_fitnesslogs( $args );
				break;

			case 'get.logs.calwidget':
				$data = $this->get_logsCalWidget( $args );
				break;

			// End of fitness_logs module

			// Module_name : Coaching
			case 'get.clientReports':
				$data = $this->get_client_reports( $args );
				break;
			//

			// Module_name : Workouts
			case 'insert.workout':
				$data = $this->insert_workout( $args );
				break;
			// End of Workout Module



			// End of New API for Adjustment
					

		endswitch;

		// Allow extensions to setup their own return data
		$this->data = apply_filters( 'um_api_output_data', $data, $query_mode, $this );

		// Log this API request, if enabled. We log it here because we have access to errors.
		$this->log_request( $this->data );

		// Send out data to the output function
		$this->output();
	}

	
	
	/**
	 * Get some stats
	 */
	public function get_stats( $args ) {
		global $wpdb, $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		$query = "SELECT COUNT(*) FROM {$wpdb->prefix}users";
		$count = absint( $wpdb->get_var($query) );
		$response['stats']['total_users'] = $count;
		
		include_once um_path . 'admin/core/um-admin-dashboard.php';
		$pending = $um_dashboard->get_pending_users_count();
		$response['stats']['pending_users'] = absint( $pending );
		
		if ( class_exists( 'UM_Notifications_API') ) {
			$query = "SELECT COUNT(*) FROM {$wpdb->prefix}um_notifications";
			$total_notifications = absint( $wpdb->get_var( $query ) );
			$response['stats']['total_notifications'] = $total_notifications;
		}
		
		if ( class_exists( 'UM_Messaging_API') ) {
			$query = "SELECT COUNT(*) FROM {$wpdb->prefix}um_conversations";
			$total_conversations = absint( $wpdb->get_var( $query ) );
			$response['stats']['total_conversations'] = $total_conversations;
			
			$query = "SELECT COUNT(*) FROM {$wpdb->prefix}um_messages";
			$total_messages = absint( $wpdb->get_var( $query ) );
			$response['stats']['total_messages'] = $total_messages;
		}
		
		if ( class_exists( 'UM_Online_API') ) {
			global $um_online;
			$total_online = count( $um_online->get_users() );
			$response['stats']['total_online'] = $total_online;
		}
		
		if ( class_exists( 'UM_Reviews_API') ) {
			$query = "SELECT COUNT(*) FROM {$wpdb->prefix}posts WHERE post_status='publish' AND post_type='um_review'";
			$total_reviews = absint( $wpdb->get_var( $query ) );
			$response['stats']['total_reviews'] = $total_reviews;
		}
		
		return $response;
	}


	
	/**
	 * Update user API query
	 */
	public function update_user( $args ) {
		global $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		if ( !$id ) {
			$error['error'] = __('You must provide a user ID','ultimatemember');
			return $error;
		}
		
		if ( !$data ) {
			$error['error'] = __('You need to provide data to update','ultimatemember');
			return $error;
		}
		
		um_fetch_user( $id );
		
		switch ( $data ) {
			case 'status':
				$ultimatemember->user->set_status( $value );
				$response['success'] = __('User status has been changed.','ultimatemember');
				break;
			case 'role':
				$ultimatemember->user->set_role( $value );
				$response['success'] = __('User level has been changed.','ultimatemember');
				break;
			case 'wp_role':
				$wp_user_object = new WP_User( $id );
				$wp_user_object->set_role( $value );
				$response['success'] = __('User WordPress role has been changed.','ultimatemember');
				break;
			default:
				update_user_meta( $id, $data, esc_attr( $value ) );
				$response['success'] = __('User meta has been changed.','ultimatemember');
				break;
		}
		
		return $response;
	}
	
	/**
	 * Process Get followers users API Request
	 */
	public function get_followers( $args ) {
		global $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		if ( !$id ) {
			$error['error'] = __('You must provide a user ID','ultimatemember');
			return $error;
		}
		
		if ( class_exists( 'UM_Followers_API' ) ) {
			global $um_followers;
			$results = $um_followers->api->followers( $id );
			if ( !$results ) {
				$error['error'] = __('No users were found','ultimatemember');
				return $error;
			}
			$response['followers']['count'] = $um_followers->api->count_followers_plain( $id );
			foreach( $results as $k => $v ) {
				$user = get_userdata( $v['user_id2'] );
				$response['followers']['users'][$k]['ID'] = $v['user_id2'];
				$response['followers']['users'][$k]['username'] = $user->user_login;
				$response['followers']['users'][$k]['display_name'] = $user->display_name;
			}
		} else {
			$error['error'] = __('Invalid request','ultimatemember');
			return $error;
		}
		
		return $response;
	}
	
	/**
	 * Process Get following users API Request
	 */
	public function get_following( $args ) {
		global $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		if ( !$id ) {
			$error['error'] = __('You must provide a user ID','ultimatemember');
			return $error;
		}
		
		if ( class_exists( 'UM_Followers_API' ) ) {
			global $um_followers;
			$results = $um_followers->api->following( $id );
			if ( !$results ) {
				$error['error'] = __('No users were found','ultimatemember');
				return $error;
			}
			$response['following']['count'] = $um_followers->api->count_following_plain( $id );
			foreach( $results as $k => $v ) {
				$user = get_userdata( $v['user_id1'] );
				$response['following']['users'][$k]['ID'] = $v['user_id1'];
				$response['following']['users'][$k]['username'] = $user->user_login;
				$response['following']['users'][$k]['display_name'] = $user->display_name;
			}
		} else {
			$error['error'] = __('Invalid request','ultimatemember');
			return $error;
		}
		
		return $response;
	}
	
	/**
	 * Process Get users API Request
	 */
	public function get_users( $args ) {
		global $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		if ( !$number )
			$number = 10;
		
		if ( !$orderby )
			$orderby = 'user_registered';
		
		if ( !$order )
			$order = 'desc';
		
		$loop_a = array('number' => $number, 'orderby' => $orderby, 'order' => $order );
		
		if ( $include ) {
			$include = explode(',', $include );
			$loop_a['include'] = $include;
		}
		
		if ( $exclude ) {
			$exclude = explode(',', $exclude );
			$loop_a['exclude'] = $exclude;
		}
		
		$loop = get_users( $loop_a );
		
		foreach( $loop as $user ) {
			
			unset( $user->data->user_status );
			unset( $user->data->user_activation_key );
			unset( $user->data->user_pass );
			
			um_fetch_user( $user->ID );
			
			foreach( $user as $key => $val ) {
				if ( $key != 'data' ) continue;
				if ( $key == 'data' ) {
					$key = 'profile';
					$val->roles = $user->roles;
					$val->first_name = um_user('first_name');
					$val->last_name = um_user('last_name');
					$val->community_role = um_user('role');
					$val->account_status = um_user('account_status');
					$val->profile_pic_original = $this->getsrc( um_user('profile_photo', 'original') );
					$val->profile_pic_normal = $this->getsrc( um_user('profile_photo', 200) );
					$val->profile_pic_small = $this->getsrc( um_user('profile_photo', 40) );
					$val->cover_photo = $this->getsrc( um_user('cover_photo', 1000) );
					
					if ( class_exists('UM_Followers_API') ) {
						global $um_followers;
						$val->followers_count = $um_followers->api->count_followers_plain( $user->ID );
						$val->following_count = $um_followers->api->count_following_plain( $user->ID );
					}
					
				}
				$response[ $user->ID ] = $val;
			}
			
		}
		
		return $response;
	}
	
	/**
	 * Process delete user via API
	 */
	public function delete_user( $args ) {
		global $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		if ( !isset( $id ) ) {
			$error['error'] = __('You must provide a user ID','ultimatemember');
			return $error;
		}
		
		$user = get_userdata( $id );
		if ( !$user ) {
			$error['error'] = __('Invalid user specified','ultimatemember');
			return $error;
		}
		
		um_fetch_user( $id );
		$ultimatemember->user->delete();
		
		$response['success'] = __('User has been successfully deleted.','ultimatemember');
		
		return $response;
	}
	
	/**
	 * Process Get user API Request
	 */
	public function get_auser( $args ) {
		global $ultimatemember;
		extract( $args );
		
		$response = array();
		$error = array();
		
		if ( !isset( $id ) ) {
			$error['error'] = __('You must provide a user ID','ultimatemember');
			return $error;
		}
		
		$user = get_userdata( $id );
		if ( !$user ) {
			$error['error'] = __('Invalid user specified','ultimatemember');
			return $error;
		}
		
		unset( $user->data->user_status );
		unset( $user->data->user_activation_key );
		unset( $user->data->user_pass );

		um_fetch_user( $user->ID );
		
		if ( isset( $fields ) && $fields ) {
			$fields = explode(',', $fields );
			$response['ID'] = $user->ID;
			$response['username'] = $user->user_login;
			foreach( $fields as $field ) {
			
				switch( $field ) {
					
					default:
						$response[$field] = (  um_profile( $field ) ) ? um_profile( $field ) : '';
						break;
						
					case 'mycred_points':
						$response['mycred_points'] = number_format( (int)get_user_meta( $user->ID, 'mycred_default', true ), 2 );
						break;
					
					case 'cover_photo':
						$response['cover_photo'] = $this->getsrc( um_user('cover_photo', 1000) );
						break;
					
					case 'profile_pic':
						$response['profile_pic_original'] = $this->getsrc( um_user('profile_photo', 'original') );
						$response['profile_pic_normal'] = $this->getsrc( um_user('profile_photo', 200) );
						$response['profile_pic_small'] = $this->getsrc( um_user('profile_photo', 40) );
						break;
						
					case 'status':
						$response['status'] = um_user('account_status');
						break;
						
					case 'role':
						$response['role'] = um_user('role');
						break;
						
					case 'email':
					case 'user_email':
						$response['email'] = um_user('user_email');
						break;
						
					case 'followers':
						if ( class_exists('UM_Followers_API') ) {
							global $um_followers;
							$response['followers_count'] = $um_followers->api->count_followers_plain( $user->ID );
							$response['following_count'] = $um_followers->api->count_following_plain( $user->ID );
						}
						break;
						
				}
				
			}
		} else {

			foreach( $user as $key => $val ) {
				if ( $key != 'data' ) continue;
				if ( $key == 'data' ) {
					$key = 'profile';
					$val->roles = $user->roles;
					$val->first_name = um_user('first_name');
					$val->last_name = um_user('last_name');
					$val->community_role = um_user('role');
					$val->account_status = um_user('account_status');
					$val->profile_pic_original = $this->getsrc( um_user('profile_photo', 'original') );
					$val->profile_pic_normal = $this->getsrc( um_user('profile_photo', 200) );
					$val->profile_pic_small = $this->getsrc( um_user('profile_photo', 40) );
					$val->cover_photo = $this->getsrc( um_user('cover_photo', 1000) );
					$val->credits=number_format( (int)get_user_meta( $user->ID, 'mycred_default', true ), 2 );	
				    $val->something= get_user_meta( $user->ID, 'user_trainer_id');		
					if ( class_exists('UM_Followers_API') ) {
						global $um_followers;
						$val->followers_count = $um_followers->api->count_followers_plain( $user->ID );
						$val->following_count = $um_followers->api->count_following_plain( $user->ID );
					}
						
				}
				$response = $val;
			}
		
		}
		
		return $response;
	}
	
	/**
	 * Get source
	 */
	public function getsrc( $image ) {
		if (preg_match('/<img.+?src(?: )*=(?: )*[\'"](.*?)[\'"]/si', $image, $arrResult)) {
			return $arrResult[1];
		}
		return '';
	}
	
	/**
	 * Determines the kind of query requested and also ensure it is a valid query
	 */
	public function get_query_mode() {
		global $wp_query;

		// Whitelist our query options
		$accepted = apply_filters( 'um_api_valid_query_modes', array(
			'get.users',
			'get.user',
			'update.user',
			'delete.user',
			'get.following',
			'get.followers',
			'get.stats',
			'is.user',
			'reset.password',
			'change.password',
			'register.user',
			'update.profile',
			'add.members',
			'get.members',
			'get.trainers',
			'get.conversation',
			'get.lastmessage',
			'get.message',
			'add.conversation',
			'add.message',
			'get.usersByRole',
			'add.group',
			'update.group',
			'user.registerToEvent',
			'get.interestCategories',
			'addconversation.group',
			'addmessage.group',
			'getconversation.group',
			'getmessage.group',
			'get.last.message.group',
			'get.userEvents',
			'get.clients',
			'accept.client',
			'fitness.data.calwidget',
			'get.upcomingEvents',
			'get.groups',
			'get.programs',
			'get.last.weight',
			'get.packages',
			'favorite.package',
			'delete.package',
			'favorite.program',
			'delete.program',
			'get.eventsByDate',
			'get.programLists',
			'favorite.group',
			'get.recentMessages',
			'get.userDetails',
			'add.program',
			'add.package',
			'get.packageDetails',
			'change.status',
			'get.clientPackage',
			'get.fullPackages',
			'get.allFood',
			'get.fitnesslogs',
			'insert.fullPackage',
			'get.allFitnesslogs',
			'get.clientReports',

			// Register User //
			'check.user.exist',
			'register.user.demo',
			'insert.initial.password',
			'check.validation.status',
			// END Register User //
			// Fetch User Details //
			'fetch.user.details',
			// END Fetch User Details //
			// Dashboard //
			'add.fitnesslogs',
			'get.logs.calwidget',
			// END Dashboard //
			// Clients //
			'get.allClients',
			'add.client',
			// END Clients //
			'add.client',
			//Start Workout
			'insert.workout',
			'get.workout'
			//End Workout
		) );

		$query = isset( $wp_query->query_vars['um-api'] ) ? $wp_query->query_vars['um-api'] : null;
		$error = array();
		// Make sure our query is valid
		if ( ! in_array( $query, $accepted ) ) {
			$error['error'] = __( 'Invalid query!', 'ultimatemember' );

			$this->data = $error;
			$this->output();
		}

		return $query;
	}

	/**
	 * Get page number
	 */
	public function get_paged() {
		global $wp_query;

		return isset( $wp_query->query_vars['page'] ) ? $wp_query->query_vars['page'] : 1;
	}

	/**
	 * Retrieve the output format
	 */
	public function get_output_format() {
		global $wp_query;

		$format = isset( $wp_query->query_vars['format'] ) ? $wp_query->query_vars['format'] : 'json';

		return apply_filters( 'um_api_output_format', $format );
	}

	/**
	 * Log each API request, if enabled
	 */
	private function log_request( $data = array() ) {
		if ( ! $this->log_requests )
			return;

	}


	/**
	 * Retrieve the output data
	 */
	public function get_output() {
		return $this->data;
	}

	/**
	 * Output Query in either JSON/XML. The query data is outputted as JSON
	 * by default
	 */
	public function output( $status_code = 200 ) {
		global $wp_query;

		$format = $this->get_output_format();

		status_header( $status_code );

		do_action( 'um_api_output_before', $this->data, $this, $format );

		switch ( $format ) :

			case 'xml' :

				require_once um_path . 'core/lib/array2xml.php';
				$xml = Array2XML::createXML( 'um', $this->data );
				echo $xml->saveXML();

				break;

			case 'json' :
			case '' :

				header( 'Content-Type: application/json' );
				if ( ! empty( $this->pretty_print ) )
					echo json_encode( $this->data, $this->pretty_print );
				else
					echo json_encode( $this->data );

				break;


			default :

				// Allow other formats to be added via extensions
				do_action( 'um_api_output_' . $format, $this->data, $this );

				break;

		endswitch;

		do_action( 'um_api_output_after', $this->data, $this, $format );

		die();
	}

	/**
	 * Modify User Profile
	 */
	function user_key_field( $user ) {
		if ( current_user_can( 'edit_users' ) && current_user_can( 'edit_user', $user->ID ) ) {
			$user = get_userdata( $user->ID );
			?>
			<table class="form-table">
				<tbody>
					<tr>
						<th>
							<label for="um_set_api_key"><?php _e( 'Ultimate Member REST API', 'ultimatemember' ); ?></label>
						</th>
						<td>
							<?php if ( empty( $user->um_user_public_key ) ) { ?>
								<p><input name="um_set_api_key" type="checkbox" id="um_set_api_key" value="0" />
								<span class="description"><?php _e( 'Generate API Key', 'ultimatemember' ); ?></span></p>
							<?php } else { ?>
								<p>
								<strong><?php _e( 'Public key:', 'ultimatemember' ); ?>&nbsp;</strong><span id="publickey"><?php echo $user->um_user_public_key; ?></span><br/>
								<strong><?php _e( 'Secret key:', 'ultimatemember' ); ?>&nbsp;</strong><span id="privatekey"><?php echo $user->um_user_secret_key; ?></span><br/>
								<strong><?php _e( 'Token:', 'ultimatemember' ); ?>&nbsp;</strong><span id="token"><?php echo $this->get_token( $user->ID ); ?></span>
								</p>
								<p><input name="um_set_api_key" type="checkbox" id="um_set_api_key" value="0" />
								<span class="description"><?php _e( 'Revoke API Keys', 'ultimatemember' ); ?></span></p>
							<?php } ?>
						</td>
					</tr>
				</tbody>
			</table>
		<?php }
	}

	/**
	 * Generate new API keys for a user
	 */
	public function generate_api_key( $user_id = 0, $regenerate = false ) {

		if( empty( $user_id ) ) {
			return false;
		}

		$user = get_userdata( $user_id );

		if( ! $user ) {
			return false;
		}

		if ( empty( $user->um_user_public_key ) ) {
			update_user_meta( $user_id, 'um_user_public_key', $this->generate_public_key( $user->user_email ) );
			update_user_meta( $user_id, 'um_user_secret_key', $this->generate_private_key( $user->ID ) );
		} elseif( $regenerate == true ) {
			$this->revoke_api_key( $user->ID );
			update_user_meta( $user_id, 'um_user_public_key', $this->generate_public_key( $user->user_email ) );
			update_user_meta( $user_id, 'um_user_secret_key', $this->generate_private_key( $user->ID ) );
		} else {
			return false;
		}

		return true;
	}

	/**
	 * Revoke a users API keys
	 */
	public function revoke_api_key( $user_id = 0 ) {

		if( empty( $user_id ) ) {
			return false;
		}

		$user = get_userdata( $user_id );

		if( ! $user ) {
			return false;
		}

		if ( ! empty( $user->um_user_public_key ) ) {
			delete_transient( md5( 'um_api_user_' . $user->um_user_public_key ) );
			delete_user_meta( $user_id, 'um_user_public_key' );
			delete_user_meta( $user_id, 'um_user_secret_key' );
		} else {
			return false;
		}

		return true;
	}


	/**
	 * Generate and Save API key
	 */
	public function update_key( $user_id ) {
		if ( current_user_can( 'edit_user', $user_id ) && isset( $_POST['um_set_api_key'] ) ) {

			$user = get_userdata( $user_id );

			if ( empty( $user->um_user_public_key ) ) {
				update_user_meta( $user_id, 'um_user_public_key', $this->generate_public_key( $user->user_email ) );
				update_user_meta( $user_id, 'um_user_secret_key', $this->generate_private_key( $user->ID ) );
			} else {
				$this->revoke_api_key( $user_id );
			}
		}
	}

	/**
	 * Generate the public key for a user
	 */
	private function generate_public_key( $user_email = '' ) {
		$auth_key = defined( 'AUTH_KEY' ) ? AUTH_KEY : '';
		$public   = hash( 'md5', $user_email . $auth_key . date( 'U' ) );
		return $public;
	}

	/**
	 * Generate the secret key for a user
	 */
	private function generate_private_key( $user_id = 0 ) {
		$auth_key = defined( 'AUTH_KEY' ) ? AUTH_KEY : '';
		$secret   = hash( 'md5', $user_id . $auth_key . date( 'U' ) );
		return $secret;
	}

	/**
	 * Retrieve the user's token
	 */
	private function get_token( $user_id = 0 ) {
		$user = get_userdata( $user_id );
		return hash( 'md5', $user->um_user_secret_key . $user->um_user_public_key );
	}

}
