<?php

//Exit if accessed directly

if ( !defined('ABSPATH') ){
    exit;
}

require_once $dir . 'vendor/autoload.php';
require_once $dir . 'fc-payment.php';
require_once $dir . 'fc-forums.php';
require_once $dir . 'fc-events-registration.php';

class FitcodeRest
{

	private $name_space;
	private $fc_payment;
	private $fc_forums;
	
	function __construct()
	{		
		$this->name_space = 'fc-api/v1';
		$this->fc_payment = new FitcodePayment();
		$this->fc_forums = new FitcodeForums();
		$this->fc_event_registration = new FitcodeEventsRegistration();
		$this->registerApiEndPoints();
	}

	# currently unused
	private function initRestOverrides(){
		$this->addPostCredits();
	}

	private function registerApiEndPoints(){
		//Register all payment related custom end points
		$this->fc_payment->registerPaymentEndPoints();
		
		//Register all forums related custom end points
		$this->fc_forums->registerForumsEndPoints();

		$this->fc_event_registration->registerEventsEndPoints();
	}

	# currently unused
	private function addPostCredits(){
		/*
	    Overrides the default rest post return obj and adds the fitsource_credits_required to it.
		*/
		add_action( 'rest_api_init', function() {
		    register_rest_field( 'post', 'credits', array(
		        'get_callback' => function( $post_arr ) {
		            $post_credits = get_post_meta( $post_arr['id'], 'fitsource_credits_required', true);
		            return $post_credits;
		        }
		    ) );
		} );
	}	
	
}