<?php

//Exit if accessed directly

if ( !defined('ABSPATH') ){
    exit;
}

// Change this with production API key upon deployment
\Stripe\Stripe::setApiKey("sk_test_H8zxUp8eFimnVWnyzr5utd42");

class FitcodePayment
{

	private $name_space;

	function __construct(){
		$this->name_space = 'fc-api/v1';
	}

	public function registerPaymentEndPoints(){

		/*
			Register route for Stripe charges
		*/ 
		add_action( 'rest_api_init', function () {
			register_rest_route(
				'fc-api/v1',
				'/stripe/charge',
				array(
					'methods' => 'POST',
					'callback' => array( &$this, 'stripe_charge_client' ),
				)
			);
		});

	}

	public function stripe_charge_client( WP_REST_Request $request ){
		$token = $request['chargeObj']['token']['id'];
		$amount = $request['chargeObj']['amount'];

		try {
			$charge = \Stripe\Charge::create(array(
				"amount" => $amount,
				"currency" => "usd",
				"description" => "Example charge",
				"source" => $token,
			));

			return $charge;
		} catch (Exception $e) {
			return $e;
		}
	}	

}