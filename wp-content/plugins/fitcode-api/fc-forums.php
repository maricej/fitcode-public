<?php

//Exit if accessed directly

if ( !defined('ABSPATH') ){
    exit;
}

/**
* 
*/
class FitcodeForums
{

	private $name_space;

	function __construct(){		
		$this->name_space = 'fc-api/v1';
	}

	/*
		Register custom routes for FC Forums
	*/ 
	public function registerForumsEndPoints(){
		
		//register end point for getting topics by forum id
		add_action( 'rest_api_init', function () {
			register_rest_route(
				$this->name_space, 
				'/forums/(?P<id>\d+)/topics',
				array(
				'methods' 	=> 'GET',
				'callback'	=> array( &$this, 'get_topics_by_parent_forum_id' ),
			));
		});

		add_action( 'rest_api_init', function () {
			register_rest_route(
				$this->name_space, 
				'/forums/topics/add',
				array(
				'methods' 	=> 'POST',
				'callback' 	=> array( &$this, 'add_topic_by_parent_forum_id' ),
			));
		});

		//register end point for getting topics by topic_id
		add_action( 'rest_api_init', function () {
			register_rest_route(
				$this->name_space, 
				'/forums/(?P<id>\d+)/topics/(?P<topic_id>\d+)',
				array(
				'methods' 	=> 'GET',
				'callback' 	=> array( &$this, 'get_topic_by_id' ),
			));
		});

		add_action( 'rest_api_init', function () {
			register_rest_route(
				$this->name_space, 
				'/forums/topics/post-comment/add',
				array(
				'methods' 	=> 'POST',
				'callback' 	=> array( &$this, 'add_topic_post_comment' ),
			));
		});

		// add_filter( 'tiny_mce_before_init', function ($initArray) {
		// 	$initArray['extended_valid_elements'] = "iframe[id|class|title|style|align|frameborder|height|longdesc|marginheight|marginwidth|name|scrolling|src|width]";

		// 	return $initArray;
		// });
		
		// add_filter( 'wp_kses_allowed_html', function ( $tags, $context ) {
		//     if ( 'post' === $context ) {
		//         $tags['iframe'] = array(
		//             'src'    => true,
		//             'srcdoc' => true,
		//             'width'  => true,
		//             'height' => true,
		//         );
		//     }
		//     return $tags;
		// }, 10, 2 );

	}

	//callback for getting topics by forum id
	public function get_topics_by_parent_forum_id( WP_REST_Request $request ){

		$args = array(
			'post_type' 	=> 'topic',
			'numberposts' 	=> -1,
			'orderby' 		=> 'meta_value',
			'order' 		=> 'ASC',
			'meta_query' 	=> array(array('key' => '_wpcf_belongs_forum_id', 'value' => $request['id']))
		);

		$child_topics = get_posts($args);
		return $child_topics;
	}
		
	//callback for adding topics by forum id
	public function add_topic_by_parent_forum_id( WP_REST_Request $request ){

		$forum_id = $request['forum_id'];
		$topic = $request['topic'];

		try {

			$postarr = array(
				'post_type' 	=> 'topic',
				'post_title' 	=> $topic,
				'post_status' 	=> 'publish',
				'meta_input'	=> array('_wpcf_belongs_forum_id' => $forum_id)
			);

			$new_topic_id = wp_insert_post($postarr);
			$new_topic = get_post($new_topic_id);

			return $new_topic;

		} catch (Exception $e) {
			return $e;
		}
		
	}

	//callback for getting topics by forum id
	public function get_topic_by_id( WP_REST_Request $request ){
		$topic = get_post($request['topic_id']);

		//Get all posts/comments for the topic 
		$args = array(
			'post_type' 	=> 'topic_post',
			'numberposts' 	=> -1,
			'orderby' 		=> 'meta_value',
			'order' 		=> 'ASC',
			'post_status' 	=> 'publish',
			'meta_query' 	=> array(array('key' => '_wpcf_belongs_topic_id', 'value' => $topic->ID))
		);

		$forum_topic_posts = get_posts($args);

		//Get topic author data for each topic post
		foreach ($forum_topic_posts as $post) {
			$post->author = new stdClass();
			$post->author->nice_name = get_the_author_meta('user_nicename', $post->post_author);
			$post->author->display_name = get_the_author_meta('display_name', $post->post_author);
			$post->author->first_name = get_the_author_meta('first_name', $post->post_author);
			$post->author->last_name = get_the_author_meta('last_name', $post->post_author);
			$post->author->avatar = get_avatar_url($post->post_author);
			$post->decoded_content = html_entity_decode($post->post_content);
   		}

		//Package and organize data before returning it
		$topic_data = new stdClass();
   		$topic_data->title = $topic->post_title;
   		$topic_data->content = $topic->post_content;
   		$topic_data->date_created = $topic->post_date;
   		$topic_data->date_updated = $topic->post_modified;
   		$topic_data->post_type = $topic->post_type;
   		$topic_data->status = $topic->post_status;
   		$topic_data->posts = $forum_topic_posts;

		return $topic_data;
	}

	//callback for adding topic post comment
	public function add_topic_post_comment( WP_REST_Request $request ){

		$new_post_comment = $request['new_post_comment'];

		try {

			$postarr = array(
				'post_type' 	=> 'topic_post',
				// 'post_content' 	=> $new_post_comment['the_comment'],
				'post_content' 	=> esc_html($new_post_comment['the_comment']),
				'post_status' 	=> 'publish',
				'meta_input' 	=> array('_wpcf_belongs_topic_id' => $new_post_comment['topic_id']),
				'post_author' 	=> $new_post_comment['user_id']
			);

			$new_topic_post_id = wp_insert_post($postarr);
			$new_topic_post = get_post($new_topic_post_id);

			// Setup new topic post author data
			$new_topic_post->author = new stdClass();
			$new_topic_post->author->nice_name = get_the_author_meta('user_nicename', $new_topic_post->post_author);
			$new_topic_post->author->display_name = get_the_author_meta('display_name', $new_topic_post->post_author);
			$new_topic_post->author->first_name = get_the_author_meta('first_name', $new_topic_post->post_author);
			$new_topic_post->author->last_name = get_the_author_meta('last_name', $new_topic_post->post_author);
			$new_topic_post->author->avatar = get_avatar_url($new_topic_post->post_author);
			$new_topic_post->decoded_content = html_entity_decode($new_topic_post->post_content);

			return $new_topic_post;

		} catch (Exception $e) {
			return $e;
		}

	}

}
