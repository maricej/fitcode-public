<?php
/**
* Plugin Name: Fitcode Rest Api
* Author: DGTL.im Team (Instigated by: Byron Alfonso)
* Plugin URI: 
* Description: This plugin serves as a wrapper to all Fitcode related custom web api. It also holds some other WP functionality and overrides.
* Author URI:
* Version: 0.0.1 
*/

//Exit if accessed directly

if ( !defined('ABSPATH') ){
	exit;
}

$dir = plugin_dir_path(__FILE__);

require_once $dir . 'fc-rest.php';

$fc_rest_obj = new FitcodeRest();